package com.floodlight.KildaEvents;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

import org.bitbucket.kilda.controller.Heartbeat;
import org.projectfloodlight.openflow.protocol.OFPortDesc;
import org.projectfloodlight.openflow.types.DatapathId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFSwitchListener;
import net.floodlightcontroller.core.PortChangeType;
import net.floodlightcontroller.core.internal.IOFSwitchService;
import net.floodlightcontroller.core.internal.OFSwitchManager;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.staticentry.IStaticEntryPusherService;

public class SwitchListenereEvents implements IOFSwitchListener, IFloodlightModule {

	protected IFloodlightProviderService floodlightProvider;
	protected IStaticEntryPusherService sfp;
	protected IOFSwitchService switchService;
	Properties properties = new Properties();

	private static final Logger log = LoggerFactory.getLogger(SwitchListenereEvents.class);

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleServices() {
		return null;
	}

	@Override
	public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
		return null;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
		Collection<Class<? extends IFloodlightService>> l = new ArrayList<Class<? extends IFloodlightService>>();
		l.add(IFloodlightProviderService.class);
		l.add(IStaticEntryPusherService.class);
		l.add(IOFSwitchService.class);
		return l;
	}

	@Override
	public void init(FloodlightModuleContext context) throws FloodlightModuleException {

		floodlightProvider = context.getServiceImpl(IFloodlightProviderService.class);
		sfp = context.getServiceImpl(IStaticEntryPusherService.class);
		switchService = context.getServiceImpl(IOFSwitchService.class);
	}

	@Override
	public void startUp(FloodlightModuleContext context) throws FloodlightModuleException {
		switchService.addOFSwitchListener(this);
		log.info("**** switches startup event triggered from Listener***");
	}

	@Override
	public void switchAdded(DatapathId switchId) {
		log.info("**** switches Add event triggered from Listener***");
		Heartbeat heartBeat = new Heartbeat(properties);
		heartBeat.createProducer("**** switches Add event triggered from Listener***").run();
	}

	@Override
	public void switchRemoved(DatapathId switchId) {
		log.info("**** switches Removed event triggered from Listener***");
		Heartbeat heartBeat = new Heartbeat(properties);
		heartBeat.createProducer("**** switches Removed event triggered from Listener***").run();
	}

	@Override
	public void switchActivated(DatapathId switchId) {
		log.info("**** switches Activated event triggered from Listener***");
		Heartbeat heartBeat = new Heartbeat(properties);
		heartBeat.createProducer("**** switches Activated event triggered from Listener***").run();
	}

	@Override
	public void switchPortChanged(DatapathId switchId, OFPortDesc port, PortChangeType type) {
		log.info("**** switches PortChanged event triggered from Listener***");
		Heartbeat heartBeat = new Heartbeat(properties);
		heartBeat.createProducer("**** switches PortChanged event triggered from Listener***").run();
	}

	@Override
	public void switchChanged(DatapathId switchId) {
		log.info("**** switches Changed event triggered from Listener***");
		Heartbeat heartBeat = new Heartbeat(properties);
		heartBeat.createProducer("**** switches Changed event triggered from Listener***").run();
	}

	@Override
	public void switchDeactivated(DatapathId switchId) {
		log.info("**** switches Deactivated event triggered from Listener***");
		Heartbeat heartBeat = new Heartbeat(properties);
		heartBeat.createProducer("**** switches Deactivated event triggered from Listener***").run();
	}
}