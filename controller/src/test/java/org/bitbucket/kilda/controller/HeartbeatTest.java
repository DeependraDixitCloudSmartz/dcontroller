package org.bitbucket.kilda.controller;

import java.time.Instant;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

public class HeartbeatTest {
	
	private static final Logger logger = LoggerFactory.getLogger(HeartbeatTest.class);
	private  String topicName;
	private  String url;
	private Properties props;
	private Instant startTime;

	@Before
	public void setUp() throws Exception {
		ImmutableMap<String, String> defaults = ImmutableMap.of("kafka.host", "127.0.0.1", "ops.heartbeat.sleep", "500",
				"ops.heartbeat.listener.commit.interval", "500", "ops.heartbeat.listener.sleep", "1500");
		 props = new Properties();
		 props.putAll(defaults);	
		url = props.getOrDefault("kafka.host", "localhost") + ":" + props.getOrDefault("kafka.port", "9092");
		topicName = (String) props.getOrDefault("ops.heartbeat.topic", "kilda.heartbeat");
		startTime = Instant.now();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createProducerTest() {
		
		Properties kprops = new Properties();
		String sleepTimeStr = (String) props.getOrDefault("ops.heartbeat.sleep", "5000");
		long sleepTime = StringUtils.isNumeric(sleepTimeStr) ? Long.decode(sleepTimeStr) : 5000L;
		kprops.put("bootstrap.servers", url);
		kprops.put("acks", props.getOrDefault("ops.heartbeat.acks", "all"));
		kprops.put("retries", props.getOrDefault("ops.heartbeat.retries", "3"));
		kprops.put("batch.size", props.getOrDefault("ops.heartbeat.batch.size", "10000"));
		kprops.put("linger.ms", props.getOrDefault("ops.heartbeat.linger.ms", "1"));
		kprops.put("buffer.memory", props.getOrDefault("ops.heartbeat.buffer.memory", "10000000"));
		kprops.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		kprops.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

	    Heartbeat hbt=new Heartbeat(kprops);
	    hbt.createProducer();
				
		/*PrintBoltTest test = Mockito.mock(PrintBoltTest.class);
		when(test.executeTest(input, collector);).thenReturn(43);
*/
		Assert.assertEquals(topicName,"kilda.heartbeat");
	}
	
	@Test
	public void createConsumer(){
		 Heartbeat hbt=new Heartbeat(props);
		 hbt.createConsumer();
	}
	}


