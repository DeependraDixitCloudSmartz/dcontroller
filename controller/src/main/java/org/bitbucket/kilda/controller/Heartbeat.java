package org.bitbucket.kilda.controller;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Properties;
import java.util.concurrent.Executors;

import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.TopologyBuilder;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.StringScheme;
import storm.kafka.ZkHosts;

/**
 * Heartbeat is a simple mechanism for monitoring health.
 *
 * It sends a record to a kafka topic every period (the period is configurable).
 *
 * REFERENCES - Producer:
 * https://kafka.apache.org/0101/javadoc/index.html?org/apache/kafka/clients/
 * producer/KafkaProducer.html - Consumer:
 * https://kafka.apache.org/0101/javadoc/index.html?org/apache/kafka/clients/
 * consumer/KafkaConsumer.html
 */
public class Heartbeat {
	
	private static final Logger logger = LoggerFactory.getLogger(Heartbeat.class);
	private final String topicName;
	private final String url;
	private final Properties props;
	private final Instant startTime;

	public Heartbeat(Properties props) {
		this.props = props;
		url = props.getOrDefault("kafka.host", "localhost") + ":" + props.getOrDefault("kafka.port", "9092");
		topicName = (String) props.getOrDefault("ops.heartbeat.topic", "kilda2");
		startTime = Instant.now();
	}

	public Runnable createProducer() {
		return this.new Producer();
	}

	// if producer is called with message that message will be send to kafka
	// server
	public Runnable createProducer(String messageToSent) {
		return this.new Producer(messageToSent);
	}

	public Runnable createConsumer() {
		return this.new Consumer();
	}

	public class Consumer implements Runnable {

		@Override
		public void run() {
		    //access kafka brokers
			ZkHosts zkHosts=new ZkHosts("192.168.75.198:2181");
	        String topic_name=topicName;
	        String consumer_group_id="id7";
	        String zookeeper_root="";
	        SpoutConfig kafkaConfig=new SpoutConfig(zkHosts, topic_name, zookeeper_root, consumer_group_id);
	        
	        kafkaConfig.scheme=new SchemeAsMultiScheme(new StringScheme());
	        kafkaConfig.forceFromStart=false;
	        
	        TopologyBuilder builder=new TopologyBuilder();
	        builder.setSpout("KafkaSpout", new KafkaSpout(kafkaConfig), 1);
	        builder.setBolt("PrinterBolt", new PrinterBolt()).globalGrouping("KafkaSpout");
	        
	        Config config=new Config();
	        
	        LocalCluster cluster=new LocalCluster();
	        
	        cluster.submitTopology("KafkaConsumerTopology", config, builder.createTopology());
	        
	        try{
	        	Thread.sleep(60000);
	        }catch(InterruptedException ex)
	        {
	        	ex.printStackTrace();
	        }
	        
	        cluster.killTopology("KafkaConsumerTopology");
	        cluster.shutdown();
		}

	}

	public class Producer implements Runnable {

		// stringMessage will contain message that will be send to Kafka
		// producer
		private String stringMessage;

		// if somebody invoke default constructor it will send starttime to
		// kafka server
		public Producer() {
			stringMessage = startTime.toString();
		}

		// if somebody invoke this constructor it will send provided message to
		// kafka server
		public Producer(String message) {
			stringMessage = message;
		}

		@Override
		public void run() {
			/*
			 * NB: This code assumes you have kafka available at the url
			 * address. This one is available through the services in this
			 * project - ie "docker-compose up zookeeper kafka" ** You'll have
			 * to put that into /etc/hosts if running this code from CLI.
			 */
			Properties kprops = new Properties();
			String sleepTimeStr = (String) props.getOrDefault("ops.heartbeat.sleep", "5000");
			long sleepTime = StringUtils.isNumeric(sleepTimeStr) ? Long.decode(sleepTimeStr) : 5000L;

			kprops.put("bootstrap.servers", url);
			kprops.put("acks", props.getOrDefault("ops.heartbeat.acks", "all"));
			kprops.put("retries", props.getOrDefault("ops.heartbeat.retries", "3"));
			kprops.put("batch.size", props.getOrDefault("ops.heartbeat.batch.size", "10000"));
			kprops.put("linger.ms", props.getOrDefault("ops.heartbeat.linger.ms", "1"));
			kprops.put("buffer.memory", props.getOrDefault("ops.heartbeat.buffer.memory", "10000000"));
			kprops.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
			kprops.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

			// TODO: use ScheduledExecutorService to scheduled fixed runnable
			// https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledExecutorService.html
			// TODO: add shutdown hook, to be polite, regarding producer.close
			Executors.newSingleThreadExecutor(new ThreadFactoryBuilder().setNameFormat("heartbeat.producer").build())
					.execute(new Runnable() {
						@Override
						public void run() {
							KafkaProducer<String, String> producer = new KafkaProducer<>(kprops);
							try {
								while (true) {
									logger.trace("==> sending record");
									 
									String now = Instant.now().toString();
									for(int i=0;i<10;i++)
									{
									producer.send(new ProducerRecord<>(topicName,now, stringMessage));
									}
									logger.debug("Sent record:  now = {}, start = {}",now, stringMessage);
									Thread.sleep(sleepTime);
									try {
										Thread.sleep(60000);
									} catch (InterruptedException ex) {
										ex.printStackTrace();
									}

								}
							} catch (InterruptedException e) {
								logger.info("Heartbeat Producer Interrupted");
							} finally {

								producer.close();
							}
						}

						
					});
		}

	}

	

	public static void main(String[] args) throws Exception {
		ImmutableMap<String, String> defaults = ImmutableMap.of("kafka.host", "127.0.0.1", "ops.heartbeat.sleep", "500",
				"ops.heartbeat.listener.commit.interval", "500", "ops.heartbeat.listener.sleep", "1500");
		Properties props = new Properties();
		props.putAll(defaults);
		Heartbeat hb = new Heartbeat(props);
		 hb.createProducer().run();
		 hb.createConsumer().run();

	}
}
